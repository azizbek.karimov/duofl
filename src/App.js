import { BrowserRouter, Route, Routes } from "react-router-dom";
import { StyledContainer } from "./components/Styled";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from "./pages/Signup";


function App() {
  return (
    <BrowserRouter>
    <StyledContainer>
      <Routes>
        <Route path="/signup" element={<Signup/>}></Route>
        <Route path="/login" element={<Login/>}></Route>
        <Route path="/dashboard" element={<Dashboard/>}></Route>
        <Route path="/" element={<Home/>}></Route>
      </Routes>
    </StyledContainer>
  </BrowserRouter>
  );
}

export default App;
