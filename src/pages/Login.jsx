import { Formik, Form } from "formik";
import * as Yup from 'yup';
import Logo from '../assets/df.jpg'
import {FiMail, FiLock} from 'react-icons/fi';
import { Circles } from 'react-loader-spinner'
import { Avatar, StyledFormArea, StyledFormButton, StyledTitle, colors, ButtonGroup, ExtraText, TextLink, CopyrightText } from "../components/Styled";
import { TextInput } from "../components/FormLib";
const Login = () => {
    return(
        <div>
            <StyledFormArea>
                <Avatar image={Logo}/>
                <StyledTitle color={colors.theme} size={30}>
                    Member login
                </StyledTitle>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                    }}
                    validationSchema={
                        Yup.object({
                            email: Yup.string().email("Invalid email adress")
                            .required("Required"),
                            password: Yup.string().min(8, "Password is too short").max(30, "Password is too long").required("Required"),
                        })
                    }
                    onSubmit={(values, { setSubmitting }) => {
                        console.log(values);
                    }}
                >
                    {({isSubmitting}) => (
                        <Form>
                            <TextInput
                                name="email"
                                type="text"
                                label="Email Address"
                                placeholder="ex@mple.com"
                                icon={<FiMail/>}
                            />

                            <TextInput
                                name="password"
                                type="password"
                                label="Password"
                                placeholder="********"
                                icon={<FiLock/>}
                            />
                            <ButtonGroup>
                                {!isSubmitting && <StyledFormButton type="submit">
                                    Login
                                </StyledFormButton>}
                                {isSubmitting && (
                                    <Circles 
                                        type="ThreeDots"
                                        color={colors.theme}
                                        height={49}
                                        width={100}
                                        />
                                )}
                            </ButtonGroup>
                        </Form>
                    )}
                </Formik>
                <ExtraText>
                    New here?<TextLink to="/signup">Signup</TextLink> 
                </ExtraText>
            </StyledFormArea>
            <CopyrightText>
                All rights reserved &copy;2023
            </CopyrightText>
        </div>
    )
}

export default Login;