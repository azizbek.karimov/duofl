
import { Formik, Form } from "formik";
import * as Yup from 'yup';
import Logo from '../assets/df.jpg';
import {FiMail, FiLock, FiUser, FiCalendar} from 'react-icons/fi';
import { Circles } from 'react-loader-spinner'
import { Avatar, StyledFormArea, StyledFormButton, StyledTitle, colors, ButtonGroup, ExtraText, TextLink, CopyrightText } from "../components/Styled";
import { TextInput } from "../components/FormLib";
const Signup = () => {
    return(
        <div>
            <StyledFormArea>
                <Avatar image={Logo}/>
                <StyledTitle color={colors.theme} size={30}>
                    Member Signup
                </StyledTitle>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        repeatPassword: "",
                        dateOfBirth: "",
                        name: ""
                    }}
                    validationSchema={
                        Yup.object({
                            email: Yup.string().email("Invalid email adress")
                            .required("Required"),
                            password: Yup.string().min(8, "Password is too short").max(30, "Password is too long").required("Required"),
                            name: Yup.string().required("Required"),
                            dateOfBirth: Yup.date().required("Required"),
                            repeatPassword: Yup.string().required("Required").oneOf([Yup.ref("password")], "Passwords must match")
                        })
                    }
                    onSubmit={(values, { setSubmitting }) => {
                        console.log(values);
                    }}
                >
                    {({isSubmitting}) => (
                        <Form>
                            <TextInput
                                name="name"
                                type="text"
                                label="Full name"
                                placeholder="Username"
                                icon={<FiUser/>}
                            />
                            <TextInput
                                name="email"
                                type="text"
                                label="Email Address"
                                placeholder="ex@mple.com"
                                icon={<FiMail/>}
                            />
                            <TextInput
                                name="dateOfBirth"
                                type="date"
                                label="Date of Birth"
                                icon={<FiCalendar/>}
                            />
                            <TextInput
                                name="password"
                                type="password"
                                label="Password"
                                placeholder="********"
                                icon={<FiLock/>}
                            />
                            <TextInput
                                name="repeatPassword"
                                type="password"
                                label="Repeat Password"
                                placeholder="********"
                                icon={<FiLock/>}
                            />
                            <ButtonGroup>
                                {!isSubmitting && <StyledFormButton type="submit">
                                    Signup
                                </StyledFormButton>}
                                {isSubmitting && (
                                    <Circles 
                                        type="ThreeDots"
                                        color={colors.theme}
                                        height={49}
                                        width={100}
                                        />
                                )}
                            </ButtonGroup>
                        </Form>
                    )}
                </Formik>
                <ExtraText>
                    Already have an account?<TextLink to="/login">Login</TextLink> 
                </ExtraText>
            </StyledFormArea>
            <CopyrightText>
                All rights reserved &copy;2023
            </CopyrightText>
        </div>
    )
}

export default Signup;